package hk.ust.cse.comp4521.TankWar;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

enum ItemType
{
    HealthPack,RapidFire,SpeedBoost;
}

public class Items {
    private ItemType type;

    private int x;
    private int y;
    private int left;
    private int right;
    private int top;
    private int bottom;
    private boolean consumed;
    private boolean expire;

    private static int OriginalCooldown=0;
    private static int OriginalMoveSpeed=0;
    private static long RapidFireStartTime=0;
    private static long SpeedBoostStartTime=0;

    private Bitmap partial;
    private int partx,party,partwidth,partheight;

    private static int RapidFireEffectTime=0;
    private static int SpeedBoostEffectTime=0;

    private long ownStartTime;

    //private static ItemsImage itemsImage=new ItemsImage();
    private static Bitmap HealthPack= BitmapFactory.decodeResource(Constants.CURRENT_CONTEXT.getResources(),R.drawable.healthpack);
    private static Bitmap RapidFire= BitmapFactory.decodeResource(Constants.CURRENT_CONTEXT.getResources(),R.drawable.rapidfire);
    private static Bitmap SpeedBoost= BitmapFactory.decodeResource(Constants.CURRENT_CONTEXT.getResources(),R.drawable.speedboost);
    public Items(ItemType type,int x,int y){
        this.type=type;
        this.x=x;
        this.y=y;
        consumed=false;
        expire=false;
        left=this.x-returnItemImage().getWidth()/2;
        right=this.x+returnItemImage().getWidth()/2;
        top=this.y-returnItemImage().getHeight()/2;
        bottom=this.y+returnItemImage().getHeight()/2;
    }

    public void collectByPlayer(Player player){
        if(Math.abs(player.getX()-x)<125&&Math.abs(player.getY()-y)<125){
            consumed=true;
            effect(player);
        }
    }

    public ItemType returnItemType(){
        return type;
    }

    public void effect(Player player){
        if(type==ItemType.HealthPack){
            int hp=player.returnTotalHealth()-player.returnHealth();
            if(hp>10){
                player.addHealth(10);
            }else player.addHealth(hp);
            consumed=true;
            expire=true;
        }
        else if(type==ItemType.RapidFire){
            ownStartTime=System.currentTimeMillis();
            RapidFireStartTime=ownStartTime;

            if(OriginalCooldown==0)
                OriginalCooldown=player.returnMaxCooldown();

            if(OriginalCooldown==player.returnMaxCooldown())
                player.setMaxCooldown(OriginalCooldown/3);

        }
        else if(type==ItemType.SpeedBoost){
            ownStartTime=System.currentTimeMillis();
            SpeedBoostStartTime=ownStartTime;

            if(OriginalMoveSpeed==0)
                OriginalMoveSpeed=player.returnMoveSpeed();

            if(OriginalMoveSpeed==player.returnMoveSpeed())
                player.setMoveSpeed(OriginalMoveSpeed/2);
        }
    }
    public void reset(Player player){
        if(type==ItemType.HealthPack){
        }
        else if(type==ItemType.RapidFire){
            player.setMaxCooldown(OriginalCooldown);
        }
        else if(type==ItemType.SpeedBoost){
            player.setMoveSpeed(OriginalMoveSpeed);
        }
    }
    public void checkExpire(Player player){
        if(consumed) {
            long startTime=type==ItemType.RapidFire?RapidFireStartTime:SpeedBoostStartTime;
            if(type==ItemType.RapidFire)RapidFireEffectTime=30-(int)Math.round((System.currentTimeMillis()-startTime)/1000);
            if(type==ItemType.SpeedBoost)SpeedBoostEffectTime=30-(int)Math.round((System.currentTimeMillis()-startTime)/1000);
            if (System.currentTimeMillis() - startTime >= 30000) {
                reset(player);
                expire = true;
            }
            if(ownStartTime!=startTime)
            {
                expire=true;
            }
        }else expire= false;
    }
    public Bitmap returnItemImage(){
        if(type==ItemType.HealthPack){
            return HealthPack;
        }
        else if(type==ItemType.RapidFire){
            return RapidFire;
        }
        else if(type==ItemType.SpeedBoost){
            return SpeedBoost;
        }
        return null;
    }

    public boolean checkWithinCanvasArea(){
        if(left<Constants.Right&&right>Constants.Left && top<Constants.Bottom && bottom>Constants.Top)
            return true;
        else return false;

    }

    public void draw(Canvas canvas){
        if(checkWithinCanvasArea()&&!consumed) {
            float canvasleft;
            float canvastop;
            if(left<Constants.Left){
                canvasleft=0;
            }else canvasleft=left-Constants.Left;
            if(top<Constants.Top){
                canvastop=0;
            }else canvastop=top-Constants.Top;

            canvas.drawBitmap(partial,canvasleft,canvastop,null);
        }
    }
    public void update(Player player){
        collectByPlayer(player);
        checkExpire(player);
        updatePartial();
        //System.out.println("OriginalMove: "+OriginalMoveSpeed+" OriginalCD "+OriginalCooldown+" player Cd "+player.returnMaxCooldown()+" player movespeed "+player.returnMoveSpeed());
    }

    private void updatePartial(){
        if(checkWithinCanvasArea()&&!consumed) {
            if (top < Constants.Top) {
                partheight = bottom - Constants.Top;
                party = Constants.Top - top;
            } else if (bottom > Constants.Bottom) {
                partheight = Constants.Bottom - top;
                party = 0;
            } else {
                partheight = returnItemImage().getHeight();
                party = 0;
            }
            if (left < Constants.Left) {
                partwidth = right - Constants.Left;
                partx = Constants.Left - left;
            } else if (right > Constants.Right) {
                partwidth = Constants.Right - left;
                partx = 0;
            } else {
                partwidth = returnItemImage().getWidth();
                partx = 0;
            }
            partial = Bitmap.createBitmap(returnItemImage(), partx, party, partwidth, partheight);
        }else partial=null;
    }
    public boolean returnExpire(){
        return expire;
    }
    public static int returnRapidFireEffectTime(){
        return RapidFireEffectTime;
    }
    public static int returnSpeedBoostEffectTime(){
        return SpeedBoostEffectTime;
    }
}
