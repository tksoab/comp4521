package hk.ust.cse.comp4521.TankWar;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class ItemsImage {
    private Bitmap HealthPack;
    private Bitmap RapidFire;
    private Bitmap SpeedBoost;
    public ItemsImage(){
        HealthPack= BitmapFactory.decodeResource(Constants.CURRENT_CONTEXT.getResources(),R.drawable.healthpack);
        RapidFire= BitmapFactory.decodeResource(Constants.CURRENT_CONTEXT.getResources(),R.drawable.rapidfire);
        SpeedBoost= BitmapFactory.decodeResource(Constants.CURRENT_CONTEXT.getResources(),R.drawable.speedboost);
    }

    public Bitmap HealthPackImage(){
        return HealthPack;
    }
    public Bitmap RapidFireImage(){
        return RapidFire;
    }
    public Bitmap SpeedBoostImage(){
        return SpeedBoost;
    }
}
